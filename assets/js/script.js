
// result info for health care
const carePUM1 = 'Person Under Monitoring (PUM)'
const carePUM2 = 'If the patient is observed to be asymptomatic with appropriate exposure history, advise him/her to self-quarantine at home or at barangay isolation units for 14 days to monitor for the development of symptoms.'
const carePUM3 = 'Inform RESU.'
const carePUM4 = 'Fill out CIF.'
const carePUM5 = 'Patient doesn\'t need testing.'

const careNeg1 = 'Not PUI Nor PUM'
const careNeg2 = 'Your patient is safe! He/She just needs to enforce social distancing and maintain good hygiene. Advise him/her to observe proper and good health maintenance.'

const careNeg1a = 'Not PUI nor PUM'
const careNeg2a = 'Refer him/her to ER or other clinic for appropriate work-up and management.'

const carePUI1 = 'Person Under Investigation (PUI)'
const carePUI2 = 'Your patient maybe a potential carrier.'
const carePUI3 = 'If patient has a mild illness, has no other underlying illness and is not older than 65 years, advise him/her to self-quarantine at home or at barangay isolation units. No testing is required. Otherwise, refer to action for severe category.'
const carePUI4 = 'If patient has a severe and/or critical illness. immediately assist them so they can be admitted to designated COVID-19 isolation area.'
const carePUI5 = 'Collect his/her serum, NPS + OPS (in VTM/UTM) and a lower respiratory tract specimen(if possible).'
const carePUI6 = 'Refer him/her to CPG'
const carePUI7 = 'If he/she refused to all necessary action, refer him/her to disposition RESU.'



// result infor for quarantine officer
const officerPUM1 = 'Person Under Monitoring (PUM)'
const officerPUM2 = 'Your patient doesn\'t have serious case, he/she just needs to monitor his/her condition from time to time. Advise him/her self-quarantine at home or at barangay isolation units.'

const officerNeg1 = 'Not PUI nor PUM' 
const officerNeg2 = 'Your patient is safe! He/she just needs to enforce social distancing and maintain good hygiene. Advise him/her to observe proper and good health maintenance.' 

const officerPUI1 = 'Person Under Investigation (PUI)'
const officerPUI2 = 'Your patient maybe a potential carrier.'
const officerPUI3 = 'If patient has a mild illness, has no other underlying illness and is not older than 65 years, advise him/her to self-quarantine at home or at barangay isolation units. No testing is required. Otherwise, refer to action for severe category.'
const officerPUI4 = 'If patient has a severe and/or critical illness, immediately assist them so they can be admitted to designated COVID-19 isolation area.'
const officerPUI5 = 'Collect his/her serum, NPS + OPS (in VTM/UTM) and a lower respiratory tract specimen (if possible).'
const officerPUI6 = 'Refer him/her to CPG.'
const officerPUI7 = 'If he/she refused to all necessary action, refer him/her to disposition RESU.'



// result infor for netizen
const netPUM1 = 'Person Under Monitoring (PUM)'
const netPUM2 = 'You don\'t have serious case, you just need to monitor your condition from time to time. You need to undergo self-quarantine at home or at barangay isolation units.'

const netNeg1 = 'Not PUI nor PUM' 
const netNeg2 = 'You are safe! You just need to enforce social distancing and maintain good hygiene. You need to observe proper and good health maintenance.' 

const netPUI1 = 'Person Under Investigation (PUI)'
const netPUI2 = 'You are maybe a potential carrier. You need to contact and coordinate with local government/barangay unit for situation.'






// use to hide some elements in screen modal
function hideElement(){
    document.querySelector('#resultSection').style.display = 'none';
    document.querySelector('#screenForm').style.display = 'none';
    document.querySelector('#citizen').style.display = 'none';
    document.querySelector('#careAcute').style.display = 'none';
    document.querySelector('#careContact').style.display = 'none';
    document.querySelector('#careOccur').style.display = 'none';
    document.querySelector('#careInfection').style.display = 'none';
    document.querySelector('#btnNext').style.display = 'none';
    document.querySelector('#btnBack').style.display = 'none';
    document.querySelector('#traveller').style.display = 'none';
    document.querySelector('#nonTravel').style.display = 'none';
    document.querySelector('#symptomsIll').style.display = 'none';
    document.querySelector('#qO').style.display = 'none';
    document.querySelector('#neti').style.display = 'none';
    document.querySelector('#nonqO').style.display = 'none';
    document.querySelector('#nonneti').style.display = 'none';
    document.querySelector('#qOFever').style.display = 'none';
    document.querySelector('#netiFever').style.display = 'none';
    document.querySelector('#qOIll').style.display = 'none';
    document.querySelector('#netiIll').style.display = 'none';
    document.querySelector('#btnCancel').innerHTML = 'Cancel';
}
hideElement();

let name, user, fever, illness, travel, providing, staying, traveling, occur, severeAcute, cluster, travHist, directCare, sameEnvi, conveyance, household, feverSympt, illSympt;
let entity = 0;




function select(name){
    let radio = document.getElementsByName(name);
    for(let x = 0; x < radio.length; x++){
        if(radio[x].checked){
            // console.log(radio[x]);
            return radio[x].value;
        }
    }
}




// use to identify which button is selected
function select(name){
    let radio = document.getElementsByName(name);
    for(let x = 0; x < radio.length; x++){
        if(radio[x].checked){
            // console.log(radio[x]);
            return radio[x].value;
        }
    }
}


select('usersName')



// use to uncheck the selected radion button once the back button is clicked
function uncheck(name){
    let radio = document.getElementsByName(name);
    for(let x = 0; x < radio.length; x++){
        if(radio[x].checked){
            return radio[x].checked = false;
        }
    }
}



// this is for cancel button
document.querySelector('#btnCancel').addEventListener('click', function(){
    uncheck('fever');
    uncheck('illness');
    uncheck('travel');
    uncheck('providing');
    uncheck('staying');
    uncheck('traveling');
    uncheck('occur');
    uncheck('severeAcute');
    uncheck('cluster');
    uncheck('travHist');
    uncheck('directCare');
    uncheck('sameEnvi');
    uncheck('conveyance');
    uncheck('household');
    uncheck('feverSympt');
    uncheck('illSympt');
    hideElement();
    document.querySelector('#screenEntity').style.display = 'block';
    document.querySelector('#uname').value = '';
    document.querySelector('p small').innerHTML = ''
    document.querySelector('p small').classList.add('alert-success');
    document.querySelector('p small').classList.remove('alert-danger'); 
})


document.querySelector('#provider').addEventListener('click', ()=>{
    document.querySelector('#screenEntity').style.display = 'none';
    document.querySelector('#screenForm').style.display = 'block';
    document.querySelector('#citizen').style.display = 'block';
    document.querySelector('#btnBack').style.display = 'block';
    document.querySelector('#btnNext').style.display = 'block';
    document.querySelector('p small').innerHTML = '';
    document.querySelector('p small').classList.remove('alert-danger');
    entity = 1;
    // console.log(entity);
})

document.querySelector('#officer').addEventListener('click', ()=>{
    document.querySelector('#screenEntity').style.display = 'none';
    document.querySelector('#screenForm').style.display = 'block';
    document.querySelector('#citizen').style.display = 'block';
    document.querySelector('#btnBack').style.display = 'block';
    document.querySelector('#btnNext').style.display = 'block';
    document.querySelector('p small').innerHTML = '';
    document.querySelector('p small').classList.remove('alert-danger');
    entity = 2;
})
document.querySelector('#netizen').addEventListener('click', ()=>{
    document.querySelector('#screenEntity').style.display = 'none';
    document.querySelector('#screenForm').style.display = 'block';
    document.querySelector('#citizen').style.display = 'block';
    document.querySelector('#btnNext').style.display = 'block';
    document.querySelector('#btnBack').style.display = 'block';
    document.querySelector('p small').innerHTML = '';
    document.querySelector('p small').classList.remove('alert-danger');
    entity = 3;
    // console.log(entity);
})

//this is for next button in screen modal
document.querySelector('#btnNext').addEventListener('click', function(){
    name = document.getElementById('uname').value;
    user = select('people');
    fever = select('fever');
    illness = select('illness');

    travel = select('travel');
    providing = select('providing');
    staying = select('staying');
    traveling = select('traveling');

    occur = select('occur');
    severeAcute = select('severeAcute');
    cluster = select('cluster');

    travHist = select('travHist');

    directCare = select('directCare');
    sameEnvi = select('sameEnvi');
    conveyance = select('conveyance');
    household = select('household');

    feverSympt = select('feverSympt');
    illSympt = select('illSympt');


    // for Health Care Provider
    if(entity === 1 && name.length > 0){
        document.querySelector('p small').innerHTML = '';
        document.querySelector('p small').classList.remove('alert-danger');
        document.querySelector('#citizen').style.display = 'none'
        document.querySelector('#careAcute').style.display = 'block';


        // this is for careAcute
        if(fever === 'No' && illness === 'No') {
            // first result in Health Care Provider
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${carePUM1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${carePUM2}</li><li>${carePUM3}</li><li>${carePUM4}</li><li>${carePUM5}</li>`;
        }
        if(fever==='Yes' && illness==='Yes' || fever==='No' && illness==='Yes' || fever==='Yes' && illness==='No'){
            document.querySelector('#careContact').style.display = 'block';
            document.querySelector('#careAcute').style.display = 'none';
            document.querySelector('#citizen').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'block';
        }


        // this is for careContact part
        if(travel === 'No' && providing === 'No' && staying === 'No' && traveling === 'No') {
            document.querySelector('#careInfection').style.display = 'block';
            document.querySelector('#careOccur').style.display = 'none';
            document.querySelector('#careContact').style.display = 'none';
        }
        if(travel === 'Yes' || travel === 'No'){
            if(providing === 'Yes' || providing === 'No'){
                if(staying === 'Yes' || staying === 'No'){
                    if (traveling === 'Yes' || traveling === 'No') {
                        document.querySelector('#careOccur').style.display = 'block';
                        document.querySelector('#careInfection').style.display = 'none';
                        document.querySelector('#careContact').style.display = 'none';
                    }
                }
            }
        }


        // this is for careProvider part
        if(severeAcute === 'No' && cluster === 'No') {
            // second result in Health Care Provider
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${careNeg1a}</li>`;
            document.querySelector('#description').innerHTML = `<li>${careNeg2a}</li>`;  
        }
        if(severeAcute==='Yes' && cluster==='Yes' || severeAcute==='Yes' && cluster==='No' || severeAcute==='No' && cluster==='Yes'){
            // third result in Health Care Provider
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${carePUI1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${carePUI2}</li><li>${carePUI3}</li><li>${carePUI4}</li><li>${carePUI5}</li><li>${carePUI6}</li><li>${carePUI7}</li>`; 
        }


        // this is for symptomsOccur part
        if(occur === 'No') {
            // fourth result in Health Care Provider
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${careNeg1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${careNeg2}</li>`; 
        }
        if(occur === 'Yes'){
            // fifth result in Health Care Provider
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${carePUI1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${carePUI2}</li><li>${carePUI3}</li><li>${carePUI4}</li><li>${carePUI5}</li><li>${carePUI6}</li><li>${carePUI7}</li>`;  
        }
    }
    // for Quarantine Officer
    else if(entity === 2 && name.length > 0){
        document.querySelector('p small').innerHTML = ''
        document.querySelector('p small').classList.remove('alert-danger');
        document.querySelector('#citizen').style.display = 'none';
        document.querySelector('#qO').style.display = 'block';
        // document.querySelector('#neti').style.display = 'none';
        document.querySelector('#traveller').style.display = 'block';

        // traveller
        if(travHist === 'Yes'){
            document.querySelector('#symptomsIll').style.display = 'block'
            document.querySelector('#qOIll').style.display = 'block'
            document.querySelector('#qOFever').style.display = 'block'
            document.querySelector('#nonTravel').style.display = 'none'
            document.querySelector('#traveller').style.display = 'none'
        }
        else if(travHist === 'No'){
            document.querySelector('#nonTravel').style.display = 'block'
            document.querySelector('#nonqO').style.display = 'block'
            document.querySelector('#symptomsIll').style.display = 'none'
            document.querySelector('#traveller').style.display = 'none'
        }
        else{}

        // non traveller
        if(directCare === 'Yes' || directCare === 'No'){
            if(sameEnvi === 'Yes' || sameEnvi === 'No'){
                if(conveyance === 'Yes' || conveyance === 'No'){
                    if(household === 'Yes' || household === 'No'){
                        document.querySelector('#symptomsIll').style.display = 'block';
                        document.querySelector('#qOIll').style.display = 'block'
                        document.querySelector('#qOFever').style.display = 'block'
                        document.querySelector('#nonTravel').style.display = 'none';
                    }
                }
            }
        }
        if(directCare === 'No' && sameEnvi === 'No' && conveyance === 'No' && household === 'No'){
            // first result in quarantine officer
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${officerNeg1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${officerNeg2}</li>`;

        }

        // symptomsIll
        if(feverSympt === 'Yes' && illSympt === 'Yes' || feverSympt === 'No' && illSympt === 'Yes' || feverSympt === 'Yes' && illSympt === 'No'){
            // second result in quarantine officer
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${officerPUI1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${officerPUI2}</li><li>${officerPUI3}</li><li>${officerPUI4}</li><li>${officerPUI5}</li><li>${officerPUI6}</li><li>${officerPUI7}</li>`;
        }
        if(feverSympt === 'No' && illSympt === 'No'){
            // third result in quarantine officer
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${officerPUM1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${officerPUM2}</li>`;
        }
    }
    // for Netizen
    else if(entity === 3 && name.length > 0){
        document.querySelector('p small').innerHTML = ''
        document.querySelector('p small').classList.remove('alert-danger');

        document.querySelector('#citizen').style.display = 'none';
        document.querySelector('#traveller').style.display = 'block';
        document.querySelector('#neti').style.display = 'block';

        // traveller
        if(travHist === 'Yes'){
            document.querySelector('#symptomsIll').style.display = 'block'
            document.querySelector('#netiFever').style.display = 'block'
            document.querySelector('#netiIll').style.display = 'block'
            document.querySelector('#nonTravel').style.display = 'none'
            document.querySelector('#traveller').style.display = 'none'
        }
        else if(travHist === 'No'){
            document.querySelector('#nonTravel').style.display = 'block'
            document.querySelector('#nonneti').style.display = 'block'
            document.querySelector('#symptomsIll').style.display = 'none'
            document.querySelector('#traveller').style.display = 'none'
        }
        else{}

        // non traveller
        if(directCare === 'Yes' || directCare === 'No'){
            if(sameEnvi === 'Yes' || sameEnvi === 'No'){
                if(conveyance === 'Yes' || conveyance === 'No'){
                    if(household === 'Yes' || household === 'No'){
                        document.querySelector('#symptomsIll').style.display = 'block';
                        document.querySelector('#netiFever').style.display = 'block';
                        document.querySelector('#netiIll').style.display = 'block';
                        document.querySelector('#nonTravel').style.display = 'none';
                    }
                }
            }
        }
        if(directCare === 'No' && sameEnvi === 'No' && conveyance === 'No' && household === 'No'){
            // first result in quarantine officer
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${officerNeg1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${officerNeg2}</li>`;

        }

        // symptomsIll
        if(feverSympt === 'Yes' && illSympt === 'Yes' || feverSympt === 'No' && illSympt === 'Yes' || feverSympt === 'Yes' && illSympt === 'No'){
            // second result in quarantine officer
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${officerPUI1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${officerPUI2}</li><li>${officerPUI3}</li><li>${officerPUI4}</li><li>${officerPUI5}</li><li>${officerPUI6}</li><li>${officerPUI7}</li>`;
        }
        if(feverSympt === 'No' && illSympt === 'No'){
            // third result in quarantine officer
            document.querySelector('#resultSection').style.display = 'block';
            document.querySelector('#screenForm').style.display = 'none';
            document.querySelector('#btnBack').style.display = 'none';
            document.querySelector('#btnNext').style.display = 'none';
            document.querySelector('#btnCancel').innerHTML = 'Close';
            document.querySelector('#lblName').innerHTML = name;
            document.querySelector('#lblName').style.textTransform = 'capitalize';
            document.querySelector('#result').innerHTML = `<li>${officerPUM1}</li>`;
            document.querySelector('#description').innerHTML = `<li>${officerPUM2}</li>`;
        }

    }
    else{
        document.querySelector('p small').innerHTML = 'Please fill out this field!'
        document.querySelector('p small').classList.add('alert-danger');
    }
})


//this is for back button in screen modal
document.querySelector('#btnBack').addEventListener('click', function(){
    travHist = select('travHist');

    if (document.querySelector('#citizen').style.display == 'block') {
        document.querySelector('#screenEntity').style.display = 'block';
        document.querySelector('#screenForm').style.display = 'none';
        document.querySelector('#btnNext').style.display = 'none';
        document.querySelector('#btnBack').style.display = 'none';
        document.querySelector('#citizen').style.display = 'none';
        document.getElementById('uname').value = '';
        entity = 0;
    }
    else if (document.querySelector('#careAcute').style.display == 'block') {
        document.querySelector('#citizen').style.display = 'block';
        document.querySelector('#btnBack').style.display = 'block';
        document.querySelector('#careAcute').style.display = 'none';
        uncheck('fever');
        uncheck('illness');
    }
    else if (document.querySelector('#careContact').style.display == 'block') {
        document.querySelector('#careAcute').style.display = 'block';
        document.querySelector('#careContact').style.display = 'none';
        uncheck('travel');
        uncheck('providing');
        uncheck('staying');
        uncheck('traveling');

    }
    else if (document.querySelector('#careOccur').style.display == 'block') {
        document.querySelector('#careContact').style.display = 'block';
        document.querySelector('#careOccur').style.display = 'none';
        uncheck('occur');

    }
    else if (document.querySelector('#careInfection').style.display == 'block') {
        document.querySelector('#careContact').style.display = 'block';
        document.querySelector('#careInfection').style.display = 'none';
        uncheck('severeAcute');
        uncheck('cluster');

    }
    else if (document.querySelector('#traveller').style.display == 'block') {
        document.querySelector('#citizen').style.display = 'block';
        document.querySelector('#traveller').style.display = 'none';
        uncheck('travHist');
    }
    else if (document.querySelector('#nonTravel').style.display == 'block') {
        document.querySelector('#traveller').style.display = 'block';
        document.querySelector('#nonTravel').style.display = 'none';
        uncheck('directCare');
        uncheck('sameEnvi');
        uncheck('conveyance');
        uncheck('household');
    }
    else if (travHist === 'Yes') {
        document.querySelector('#traveller').style.display = 'block';
        document.querySelector('#symptomsIll').style.display = 'none';
        uncheck('feverSympt');
        uncheck('illSympt');
    }
    else if (travHist === 'No') {
        document.querySelector('#nonTravel').style.display = 'block';
        document.querySelector('#symptomsIll').style.display = 'none';
        uncheck('feverSympt');
        uncheck('illSympt');
    }
    else{}
})
